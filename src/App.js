import './App.css';
import Menu from './components/nav/Menu'
import BlackScholesMerton from './components/cal-panel/BlackScholesMerton'
import BlackModel from './components/cal-panel/BlackModel'
import BinomialTree from './components/cal-panel/BinomialTree'
import Result from './components/res-panel/Result'
import { CalculatorContext } from './context/CalculatorContext'
import { useContext, useEffect, useState } from 'react'
import Spline from '@splinetool/react-spline'

function App() {

  const { calculatorState: {
    currentCalculator
  } } = useContext(CalculatorContext)

  const [currCal, setCurrCal] = useState((<BlackScholesMerton></BlackScholesMerton>))

  useEffect(() => {
    console.log('current calculator ', currentCalculator)
    switch (currentCalculator) {
      case 1:
        setCurrCal((<BlackScholesMerton></BlackScholesMerton>))
        break;
      case 2:
        setCurrCal((<BlackModel></BlackModel>))
        break;
      case 3:
        setCurrCal((<BinomialTree></BinomialTree>))
        break;
      default:
        break;
    }
  }, [currentCalculator])


  return (
    <div className='App'>
      <div id="background_spline">
        <Spline scene="https://prod.spline.design/7FKUn07JxgfOtm0X/scene.splinecode" />
      </div>
      <div id='page_content' className="grid md:grid-cols-4 gap-4 pt-5">
        <div id='calculator-menu' className="backdrop-blur-sm bg-white/10 h-fit  p-6 border border-gray-200 rounded-lg shadow-md">
          <Menu></Menu>
        </div>
        <div id='calculator-panel' className='backdrop-blur-sm bg-white/10 h-fit md:col-span-2 p-6  border border-gray-200 rounded-lg shadow-md'>
          {currCal}
        </div>
        <div id='result-panel' className='backdrop-blur-sm bg-white/10 h-fit p-6 border border-gray-200 rounded-lg shadow-md'>
          <Result></Result>
        </div>
      </div>
    </div>
  );
}

export default App;
