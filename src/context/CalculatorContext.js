import { createContext, useState } from 'react'


export const CalculatorContext = createContext()


const CalculatorContextProvider = ({ children }) => {

    const [calculatorState, setCalculatorState] = useState({
        currentCalculator: 1,
        result: null
    })

    // resArr [...{ prop: data }]
    const postRes = (res) => {
        setCalculatorState({ ...calculatorState, result: res })
    }

    // change calculator
    const changeCalculator = (cal) => {
        setCalculatorState({ ...calculatorState, currentCalculator: cal })
    }


    const CalculatorContextData = { postRes, changeCalculator, calculatorState }
    // Return provider
    return (
        <CalculatorContext.Provider value={CalculatorContextData}>
            {children}
        </CalculatorContext.Provider>
    )
}

export default CalculatorContextProvider
