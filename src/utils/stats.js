import {
    sqrt, erf
} from 'mathjs'


function cdfNormal(x, mean, std) {
    return (1 - erf((mean - x) / (sqrt(2) * std))) / 2
}

function N(x) {
    return cdfNormal(x, 0, 1)
}

export { cdfNormal, N }