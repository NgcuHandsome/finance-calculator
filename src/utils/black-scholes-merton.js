import {
    e, log, pow, sqrt
} from 'mathjs'

import { N } from './stats'

export default function caculator(params) {

    let { spotPrice, strikePrice, timeExpire, volatility, riskFree, dividends } = params

    riskFree /= 100
    volatility /= 100
    // reduce spot price by dividends
    spotPrice = dividends.reduce((curr, next) => curr -= next.dividend * pow(e, -riskFree * next.at), spotPrice)
    //
    const d1 = (log(spotPrice / strikePrice) + (riskFree + pow(volatility, 2) / 2) * timeExpire) / (volatility * sqrt(timeExpire))

    const d2 = d1 - volatility * sqrt(timeExpire)

    const callOptionPrice = spotPrice * N(d1) - pow(e, -riskFree * timeExpire) * strikePrice * N(d2)

    const putOptionPrice = -spotPrice * N(-d1) + pow(e, -riskFree * timeExpire) * strikePrice * N(-d2)

    return { callOptionPrice, putOptionPrice, d1, d2, N_d1: N(d1), N_d2: N(d2), }
}