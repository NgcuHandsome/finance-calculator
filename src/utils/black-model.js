import {
    e, log, pow, sqrt
} from 'mathjs'

import { N } from './stats'

export default function caculator(params) {
    let { futurePrice, strikePrice, timeExpire, volatility, riskFree } = params
    riskFree /= 100
    volatility /= 100
    //
    const d1 = (log(futurePrice / strikePrice) + (pow(volatility, 2) / 2) * timeExpire) / (volatility * sqrt(timeExpire))

    const d2 = d1 - volatility * sqrt(timeExpire)

    const callOptionPrice = pow(e, -riskFree * timeExpire) * (futurePrice * N(d1) - strikePrice * N(d2))

    const putOptionPrice = pow(e, -riskFree * timeExpire) * (-futurePrice * N(-d1) + strikePrice * N(-d2))

    return { callOptionPrice, putOptionPrice, d1, d2, N_d1: N(d1), N_d2: N(d2) }
}