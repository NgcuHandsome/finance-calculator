import React from 'react'
import { useState, useContext } from 'react'
import { CalculatorContext } from '../../context/CalculatorContext'

const Menu = () => {

    const [active, setActive] = useState(1)

    const { changeCalculator } = useContext(CalculatorContext)

    const onClick = (tag) => {
        setActive(tag)
        changeCalculator(tag)
    }

    return (
        <ul className="divide-y divide-gray-200">
            <li>
                <div
                    style={{
                        backgroundColor: active === 1 ? '#d6d6d67d' : ''
                    }}
                    onClick={() => onClick(1)} className="flex cursor-pointer items-center p-2 text-base font-normal text-gray-900 rounded-sm dark:text-black hover:bg-gray-100 dark:hover:bg-gray-100 ">
                    <span className="ml-2 font-medium text-sm">Black-Sholes-Merton-Model</span>
                </div>
            </li>
            <li>
                <div
                    style={{
                        backgroundColor: active === 2 ? '#d6d6d67d' : ''
                    }}
                    onClick={() => onClick(2)}
                    className="flex cursor-pointer items-center p-2 text-base font-normal text-gray-900 rounded-sm  dark:text-black hover:bg-gray-100 dark:hover:bg-gray-100 ">
                    <span className="ml-2 font-medium text-sm">Black's-Model</span>
                </div>
            </li>
            <li>
                <div
                    style={{
                        backgroundColor: active === 3 ? '#d6d6d67d' : ''
                    }}
                    onClick={() => onClick(3)}
                    className="flex cursor-pointer items-center p-2 text-base font-normal text-gray-900 rounded-sm  dark:text-black hover:bg-gray-100 dark:hover:bg-gray-100 ">
                    <span className="ml-2 font-medium text-sm">Binomial Trees</span>
                </div>
            </li>
        </ul >
    )
}

export default Menu