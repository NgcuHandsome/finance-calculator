import React from 'react'
import { useState, useContext } from 'react'
import caculator from '../../utils/black-scholes-merton'
import { CalculatorContext } from '../../context/CalculatorContext'

const BlackScholesMerton = () => {

    const [devidendForms, setDevidendForms] = useState([])

    const { postRes } = useContext(CalculatorContext)

    const [mathData, setMathData] = useState({
        spotPrice: 0,
        strikePrice: 0,
        timeExpire: 0,
        volatility: 0,
        riskFree: 0
    })

    const onChangeMathData = event => {
        setMathData({
            ...mathData,
            [event.target.name]: event.target.value
        })
    }

    const createInputDividend = (number) => {
        var newDevidendForms = []
        for (let index = 0; index < number; index++) {
            const rowInputDividend = (
                <div className='grid gap-6 mb-6 md:grid-cols-4' key={index}>
                    <div className='col-span-3'>
                        <label htmlFor="dividend" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Dividend</label>
                        <input type="number" step="any" id="dividend" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="1.00" required />
                    </div>
                    <div className='col-span-1'>
                        <label htmlFor="yield_at" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">At (Years)</label>
                        <input type="number" step="any" id="yield_at" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="0.25" required />
                    </div>
                </div>)

            newDevidendForms.push(rowInputDividend)
        }
        setDevidendForms([...newDevidendForms])
    }

    const changeDevidend = (e, action) => {
        let currentLen = devidendForms.length
        switch (action) {
            case "add":
                currentLen++
                createInputDividend(currentLen)
                break;
            case "remove":
                currentLen--
                if (!devidendForms.length < 0) return
                createInputDividend(currentLen)
                break;
            default:
                break;
        }
        e.preventDefault()
    }

    const caculate = (e) => {
        e.preventDefault()
        const dividendArr = [...e.target.querySelectorAll('#dividend')].map(i => i.value)
        const atArr = [...e.target.querySelectorAll('#yield_at')].map(i => i.value)

        const dividendData = dividendArr.map((d, i) => {
            return {
                dividend: d,
                at: atArr[i]
            }
        })

        const inputMath = {
            ...mathData, dividends: dividendData
        }

        const res = caculator(inputMath)

        postRes(res)
    }

    return (
        <form onSubmit={caculate}>
            <div className="grid gap-6 mb-6 md:grid-cols-2">
                <div>
                    <label htmlFor="spot_price" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Spot Price (S0)</label>
                    <input name='spotPrice' onChange={onChangeMathData} value={mathData.spotPrice || ''} type="number" step="any" id="spot_price" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="10.00" required />
                </div>
                <div>
                    <label htmlFor="strike_price" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Strike Price (K)</label>
                    <input name='strikePrice' onChange={onChangeMathData} value={mathData.strikePrice || ''} type="number" step="any" id="strike_price" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="8.00" required />
                </div>
                <div>
                    <label htmlFor="time_expire" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Time to Expiration (Years)</label>
                    <input name='timeExpire' onChange={onChangeMathData} value={mathData.timeExpire || ''} type="number" step="any" id="time_expire" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="0.5" required />
                </div>
                <div>
                    <label htmlFor="volatility " className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Volatility (%)</label>
                    <input name='volatility' onChange={onChangeMathData} value={mathData.volatility || ''} type="number" step="any" id="volatility" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="30" required />
                </div>
                <div>
                    <label htmlFor="risk_free" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Risk-Free Interest Rate (%)</label>
                    <input name='riskFree' onChange={onChangeMathData} value={mathData.riskFree || ''} type="number" step="any" id="risk_free" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="10" required />
                </div>
            </div>
            <div className='rounded-sm border px-2 py-2'>
                <div className='text-lg font-medium text-gray-900 dark:text-black'>Dividend</div>
                <div className="grid grid-flow-row auto-rows-max my-2">
                    {devidendForms}
                </div>
                <div className='flex justify-center gap-x-2'>
                    <button type="button" onClick={(e) => changeDevidend(e, 'add')} className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Add</button>
                    <button type="button" onClick={(e) => changeDevidend(e, 'remove')} className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Remove</button>
                </div>
            </div>
            <div className='my-4'>
                <button type="submit" className="text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Caculate</button>
            </div>
        </form >

    )
}

export default BlackScholesMerton