import React from 'react'
import { CalculatorContext } from '../../context/CalculatorContext'
import { useContext, useState } from 'react'
import caculator from '../../utils/black-model'

const BlackModel = () => {

    const { postRes } = useContext(CalculatorContext)

    const [mathData, setMathData] = useState({
        futurePrice: 0,
        strikePrice: 0,
        timeExpire: 0,
        volatility: 0,
        riskFree: 0
    })

    const onChangeMathData = event => {
        setMathData({
            ...mathData,
            [event.target.name]: event.target.value
        })
    }


    const caculate = (e) => {
        e.preventDefault()


        const inputMath = {
            ...mathData
        }

        const res = caculator(inputMath)

        postRes(res)
    }

    return (
        <form onSubmit={caculate}>
            <div className="grid gap-6 mb-6 md:grid-cols-2">
                <div>
                    <label htmlFor="future_price" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Future Price (F)</label>
                    <input name='futurePrice' onChange={onChangeMathData} type="number" step="any" id="future_price" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="10.00" required />
                </div>
                <div>
                    <label htmlFor="strike_price" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Strike Price (K)</label>
                    <input name='strikePrice' onChange={onChangeMathData} type="number" step="any" id="strike_price" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="8.00" required />
                </div>
                <div>
                    <label htmlFor="time_expire" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Time to Expiration (Years)</label>
                    <input name='timeExpire' onChange={onChangeMathData} type="number" step="any" id="time_expire" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="0.5" required />
                </div>
                <div>
                    <label htmlFor="volatility " className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Volatility (%)</label>
                    <input name='volatility' onChange={onChangeMathData} type="number" step="any" id="volatility" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="30" required />
                </div>
                <div>
                    <label htmlFor="risk_free" className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">Risk-Free Interest Rate (%)</label>
                    <input name='riskFree' onChange={onChangeMathData} type="number" step="any" id="risk_free" className="bg-white-100 border border-gray-100 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="10" required />
                </div>
            </div>
            <button type="submit" className="text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Caculate</button>
        </form>

    )
}

export default BlackModel