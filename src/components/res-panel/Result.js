import React from 'react'
import { useEffect, useContext, useState } from 'react'
import { CalculatorContext } from '../../context/CalculatorContext'
import { round } from 'mathjs'

const Result = () => {

    const { calculatorState } = useContext(CalculatorContext)

    const [resList, setReslist] = useState([])

    useEffect(() => {
        const newResList = []
        if (!calculatorState.result) return
        // map value
        Object.keys(calculatorState.result).forEach(k => {
            const item = (
                <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700" key={k}>
                    <th scope="row" className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {k}
                    </th>
                    <td className="py-4 px-6">
                        {round(calculatorState.result[k], 2)}
                    </td>
                </tr>)
            newResList.push(item)
        })
        setReslist(newResList)
    }, [calculatorState.result])


    return (
        <div>
            <div className='font-medium'>Result</div>
            <div className="overflow-x-auto relative my-2">
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-black uppercase  dark:bg-gray-100 dark:text-black">
                        <tr>
                            <th scope="col" className="py-3 px-6">
                                Variable
                            </th>
                            <th scope="col" className="py-3 px-6">
                                Value
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {resList}
                    </tbody>
                </table>
            </div>

        </div>
    )
}

export default Result