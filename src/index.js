import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import CalculatorContextProvider from './context/CalculatorContext';


const rootElement = document.getElementById('root')

var root

if (rootElement.hasChildNodes()) {
  root = ReactDOM.hydrateRoot(rootElement)
} else {
  root = ReactDOM.createRoot(rootElement)
}

root.render(
  <React.StrictMode>
    <CalculatorContextProvider>
      <App />
    </CalculatorContextProvider>
  </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
